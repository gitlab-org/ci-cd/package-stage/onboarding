# Onboarding - Package Stage

The onboarding process should provide the right tools to new team members to become productive in a sensible timeline.

This project helps the Package stage onboard its new members.


## Onboarding templates


Onboarding templates are used to create onboarding issues which complement the [general onboarding](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues) issues. 

According to the role of the new team member, multiple issue templates can be used for the Package onboarding.


- [Package stage onboarding](.gitlab/issue_templates/package-stage-onboarding.md) - For everyone
- [Package Registry onboarding](.gitlab/issue_templates/engineering-onboarding-package-registry.md) - For engineers working mostly on the package registries
- [Container Registry onboarding](.gitlab/issue_templates/engineering-onboarding-container-registry.md) - For engineers working mostly on the container registry
- [EM onboarding](.gitlab/issue_templates/package-em-onboarding.md) - For managers
