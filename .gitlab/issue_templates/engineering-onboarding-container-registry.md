## `ADD FIRST NAME`, Welcome to the GitLab Container Registry!

We're excited you'll join the Package team and work on the [Container Registry](https://gitlab.com/gitlab-org/container-registry). This document is intended to be a reference point for you as you go through your technical engineering onboarding and start to ramp up over the first couple of months. Consider this document a reference. There are a lot of links here to various issues and pages that will be useful as you get comfortable with the project. This is **your** issue, so feel free to make changes, add comments, or keep track of anything you want to review with your manager.

For Package stage onboarding information, please refer to your [package onboarding issue][package-onboarding-issue].

---
### Day 1

<details>
<summary>Manager Tasks</summary>

1. [ ] Add new team member to google and gitlab groups
      1. [ ] https://groups.google.com/a/gitlab.com/g/eng-dev-package-container-registry - Container Registry group
      1. [ ] https://gitlab.com/groups/gitlab-org/ci-cd/package-stage/container-registry-group/-/group_members - Container Registry group
1. [ ] Invite new team member to the slack channels
    1. [ ] #g_container-registry
    1. [ ] #g_container-registry_standups
1. [ ] Add new member to [Group standup](https://app.geekbot.com/dashboard/) 
1. [ ] Add new member to monthly async retrospective [participants](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml) list and to the [project team](https://gitlab.com/gl-retrospectives/package-stage/container-registry/activity) 

</details>

## 🖥 Features Presentation

1. [ ] Before diving into features, make sure you read the [Package Team group page](https://about.gitlab.com/handbook/engineering/development/ops/package/). This is also listed in your general onboarding in week 3.
1. [ ] Refer to the [Container Registry Handbook Page](https://about.gitlab.com/handbook/engineering/development/ops/package/product/container-registry) for more information.

The Container Registry side of the Package team is responsible for 2 distinct parts:

1. [ ] [The Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry).
1. [ ] [The Dependency Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/).


### Helpful Reading Material

This material gives an excellent background on why, when, and how the container registry works and the near future work to be done.

1. [ ] [Historical Context](https://about.gitlab.com/handbook/engineering/development/ops/package/product/container-registry/#historical-context)
1. [ ] [Observability](https://about.gitlab.com/handbook/engineering/development/ops/package/product/container-registry/#observability)


## 🛳 Engineering onboarding

This technical onboarding will get you through:

1. Some reading to acquire minimal context knowledge around how we work.
1. Operational tasks to set up some tools and show you our dashboards.
1. Setting up a local environment.
1. Playing with our features using your local environment.
1. Your first actual contributions.

### 📖 Documentation

1. [ ] Read [GitLab architecture overview](https://docs.gitlab.com/ee/development/architecture.html).
    * We work mainly in the Registry application (Go) and in the Rails backend (Puma).

Let's now learn about the ecosystem that the container registry is part of, as well architecture and guides specific to GitLab

1. [ ] [OCI Image Spec](https://github.com/opencontainers/image-spec/blob/main/manifest.md) - Learn about the Open Containers Image specification. 
   1. [ ] (Optional) The first ~6:30 minutes of [this video](https://www.youtube.com/watch?v=ExyWAhS2zBA) explain the spec concisely.
   1. [ ] (Optional) Watch this [practical demo](https://community.cncf.io/events/details/cncf-malaga-presents-cloud-native-talks-container-registries-are-not-just-for-containers/) where João Pereira goes through what a container image is and how the API works (using Docker first and then making requests manually with HTTPie).
1. [ ] [Token Authentication Spec](https://docs.docker.com/registry/spec/auth/) - How does the authentication flow work with clients, for example, when using `docker login`.
1. [ ] [Push/Pull Request Flow](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/push-pull-request-flow.md) - How do clients talk to the registry.
1. [ ] [Architecture Blueprint](https://docs.gitlab.com/ee/architecture/blueprints/container_registry_metadata_database) for the metadata database.

Lastly, let's focus on the Container Registry code area specifically with:

1. [ ] [Database Development Guidelines](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/database-dev-guidelines.md)
2. [ ] [Database Migrations Guide](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/database-migrations.md).
1. [ ] Familiarize yourself with the location of the [Standards for the Go application](https://about.gitlab.com/handbook/engineering/development/ops/package/container-registry/#standards) to which the registry adheres. This will remain a valuable reference as you work on the registry.
1. [ ] Read the [Contributing](https://gitlab.com/gitlab-org/container-registry/-/tree/master/docs-gitlab#contributing) section of the README
1. [ ] Read the [Releases](https://gitlab.com/gitlab-org/container-registry/-/tree/master/docs-gitlab#releases) section of the README

If you will be working on the Rails side of the Container Registry, let's focus on the Rails backend for the next reads:

* [ ] Read [Reusing Abstractions](https://docs.gitlab.com/ee/development/reusing_abstractions.html).
* [ ] Read [Feature flags for GitLab development](https://docs.gitlab.com/ee/development/feature_flags/).
  * Feature flags are a powerful piece of logic that we extensively use to deploy our most impactful changes slowly.
* [ ] Read the [Sidekiq guides](https://docs.gitlab.com/ee/development/sidekiq/).
  * We are also heavy users of background jobs. Given the complexity, we use a few custom options on top of standard Sidekiq jobs. Again, don't try to get every detail.
* [ ] Have a look at the core model file for the Container Registry: the [container repository model](https://gitlab.com/gitlab-org/gitlab/-/blob/3f76455ac9cf90a927767e55c837d6b07af818df/app/models/container_repository.rb#L3).
* [ ] Look through the main [client code](https://gitlab.com/gitlab-org/gitlab/-/blob/071a1b94fb0ae652976c4b49247a0834ba349c61/lib/container_registry/client.rb#L4) where rails talks to the container registry and all of the [surrounding classes](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/container_registry).
* [ ] Have a look at the [developer documentation for the dependency proxy](https://docs.gitlab.com/ee/development/packages/dependency_proxy.html).
* [ ] Have a look at the [controller file](https://gitlab.com/gitlab-org/gitlab/-/blob/67bf87155bf6f0d9d510ce3a7f27c204fde1507b/app/controllers/groups/dependency_proxy_for_containers_controller.rb) for the Dependency proxy.
* [ ] Look at the [dependency proxy authentication service](https://gitlab.com/gitlab-org/gitlab/-/blob/67bf87155bf6f0d9d510ce3a7f27c204fde1507b/app/services/auth/dependency_proxy_authentication_service.rb).
* [ ] (optional) Take a look at the Container registry and Dependency Proxy settings in the rails service https://docs.gitlab.com/ee/development/packages/settings#container-registry
* [ ] (optional) Take a look at the Container registry and Dependency Proxy schema diagrams for the rails service https://docs.gitlab.com/ee/development/packages/structure#container-registry

Now that you know more about the Container Registry and its architecture, let's look at our code review guidelines. Any of your contributions will need a review, so it's better to have a good understanding:

1. [ ] Read the [Code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
    1. [ ] Pay attention to the [The responsibility of the merge request author](https://docs.gitlab.com/ee/development/code_review.html#the-responsibility-of-the-merge-request-author) section.
1. [ ] Make sure you read [Database Review Guidelines](https://docs.gitlab.com/ee/development/database_review.html).
    * Be aware of [best practices](https://docs.gitlab.com/ee/development/database/#best-practices). This is not only useful for the Rails side but also for the Go application, as its database development adheres to the same practices.
    * Get a feeling for the database constraints we work with (for example: we don't have downtime during migrations).
1. [ ] Become a reviewer for the Container Registry project.
      1. Locate your [data file in the handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/team_members/person/).
      1. Add/update the projects section with the following lines:

         ```yaml
         projects:
           container-registry: reviewer
         ```

1. [ ] Senior backend engineers [should become trainee maintainers](https://about.gitlab.com/handbook/engineering/workflow/code-review/#senior-maintainers).
    1. Read [how to become a maintainer](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/becoming-a-maintainer.md) for the Container Registry.
    1. Open a [trainee maintainer issue](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/becoming-a-maintainer.md#trainee-maintainer) as specified in the documentation.
    1. Locate your [data file in the handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/team_members/person/).
    1. Update the projects section with the following lines:

         ```yaml
         projects:
           container-registry: trainee_maintainer
         ```
### 🏗  Operational tasks

1. [ ] Join the `#g_container-registry` on slack. This channel should contain most discussions specific to the container registry group and includes bookmarks of helpful links.
1. [ ] Join the #backend or #golang channel on slack. Backend contain most discussion around GitLab Rails and Golang most discussion about Go (not necessarily GitLab related).
1. [ ] Create an [access request](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/) to get access to Teleport for GitLab Registry Database. The access should be for `GitLab - Package Engineers - Console Access` Okta group
1. [ ] Familiarize yourself with [metrics and dashboards](https://about.gitlab.com/handbook/engineering/development/ops/package/container-registry/#observability).
1. [ ] Make sure you can access the [logs](https://about.gitlab.com/handbook/engineering/development/ops/package/product/container-registry/#logs).
1. [ ] Ensure that you can access the container registry project on [Sentry](https://sentry.gitlab.net/gitlab/container-registry/). Consider subscribing to events to get an email notification whenever an error is reported. To do so, visit the [notification settings](https://sentry.gitlab.net/settings/account/notifications/alerts/) on your account and make sure that the `Send Me Project Alerts` toggle is enabled. On the same page, you should locate `container-registry` in the projects list and set it to `On`.

### ⚙ Local Development Environment

Enough of our readings for now. Time to get our hands on the code.

You're free to set up your local installation as you want, the container registry can be installed with the GitLab application together or as standalone.

#### Standalone

This is the simplest setup and, therefore, the best to get you started.

1. [ ] Work through setting up a [local development environment](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/development-environment-setup.md) using the local filesystem storage backend and without the metadata database.

#### GDK

For situations where you will need to interact with GitLab, we recommend using the [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit). For example, to see the container repositories that belong to a project in the UI, you will need a local instance of GitLab (and its components) and the container registry.

If you recall the [GitLab architecture overview](https://docs.gitlab.com/ee/development/architecture.html), you will notice that GitLab is made up of many, many components.
Installing and configuring each component one by one is possible *but* that's exactly the goal of the GDK: it will install, configure and maintain all the components for you. There is even a [one-line installation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation).

Without further ado, let's:

1. [ ] [Install GDK](https://gitlab.com/gitlab-org/gitlab-development-kit#installation).

Once GDK is installed, set up the additional parts that we use in the Container Registry.

1. [ ] [Object Storage](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/object_storage.md).
1. [ ] [Container Registry](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/registry.md).
1. [ ] [Dependency Proxy](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/dependency_proxy.md).


### 🍿 Let's play around

Now that we have everything set up let's play around with our local Container Registry.

1. [ ] You will need a Project for this part. [Create one project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project) in your local instance.
1. [ ] [Buid a new container image and publish it](https://docs.gitlab.com/ee/user/packages/container_registry/#build-and-push-images-by-using-docker-commands) in your local GDK instance.
1. [ ] Open the Project on your local GDK instance and browse the Container Registry. Your brand new container image should be listed there. Click on it to see more details.
1. [ ] Bonus points: Explore the `ContainerRepository` model.

Now let's experiment with the CLI

1. [ ] Do some pushes and pulls to the locally running registry using the [`docker` CLI](https://docs.docker.com/engine/reference/commandline/cli/). Try to identify all the involved HTTP requests for each command in the registry logs. Refer to the [Push/Pull Request Flow](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/push-pull-request-flow.md) and [HTTP API Specification](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/api.md) documentation for guidance.
1. [ ] Enable authentication in your [registry configuration](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/configuration.md#auth) with the `silly:` method to learn how to login and make authenticated requests to the registry.
1. [ ] Create a Google Cloud Storage (GCS) bucket in the [`gitlab-internal`](https://console.cloud.google.com/home/dashboard?project=gitlab-internal-153318) project. GCS is the storage backend used for GitLab.com, so it is important to see how it works.
1. [ ] Configure and restart your local registry to use this bucket as the storage backend and repeat the `tag`/`push`/`pull` Docker commands and inspect the changes on the GCS bucket.

Time to play with the Dependency Proxy:

1. [ ] You will need a Group for this part. [Create one group](https://docs.gitlab.com/ee/user/group/#create-a-group) in your local instance.
1. [ ] [Manually pull](https://docs.gitlab.com/ee/user/packages/dependency_proxy/#use-the-dependency-proxy-for-docker-images) a container image (eg. `alpine:latest`) from DockerHub through the dependency proxy.
1. [ ] [Check the Dependency Proxy Storage Use](https://docs.gitlab.com/ee/user/packages/dependency_proxy/reduce_dependency_proxy_storage.html#check-dependency-proxy-storage-use) as of now, there will be some entries in the cache.
1. [ ] Bonus points: Explore the `Groups::DependencyProxyForContainersController` controller.

At this point, feel free to explore our code area in the Rails backend code source.


#### Getting Started Tasks

Here are a couple of tasks that I’ve found that you can take on when you’re ready to start getting into code. These should be fairly straightforward, but if they turn out to be too complex, let me know. These first tasks aren’t intended to be complex features, they are intended to be simple tasks that will help you get familiar with the process of shipping code at GitLab. Feel free to go through the tasks in any order you see fit while being mindful of which [milestones](https://docs.gitlab.com/ee/user/project/milestones/) they are in and their [release scopes](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#release-scoping-labels).

`onboarding buddy to add 2 to 3 issues`

### Advanced experimentation

Please consider trying some of the following experiments after completing your onboarding tasks. These should help illustrate some of the finer points
of the container registry.

1. [ ] Interacting with the GitLab Rails `/jwt/auth` endpoint using `curl` to obtain a JWT token. Refer to the [Authentication Flow](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/auth-request-flow.md) for more details.
1. [ ] Configuring GDK to use the [local registry instance](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/registry.md#configuring-the-registry).
1. [ ] Manually push and pull an image using `curl`. Refer to the [Push/Pull Request Flow](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/push-pull-request-flow.md) and [HTTP API Specification](https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs/spec/api.md) documentation for guidance.
1. [ ] (Optional) Writing your own client could be a fun way to learn the full push/pull/auth flow. Here is a sample [registry client written in Go](https://gitlab.com/jaime/registry-client) that does that.
1. [ ] (Optional) If you prefer a "UI friendly" alternative to `curl`, then consider using [postman](https://www.postman.com/) to obtain a JWT token and to interact with the container registry (i.e. push/pull) as highlighted in the tasks above. You can find a set of registry postman API requests [here](https://gitlab.com/suleimiahmed/registry-postman). You are welcome to suggest improvements to the registry postman library with an MR or an issue in the repository.

### What's next?


After completing your onboarding and getting started tasks, you should feel comfortable to contribute to the teams' milestone efforts.
Feel free to reach out to your manager or peers for guidance if you don't know how/what to do.

1. [ ] Review the current milestone's planning issue and pick an issue to start working on it!
1. [ ] Close this issue 🎉

Congratulations and welcome to the container registry!

---

### Additional Notes

Feel free to edit this issue, add any notes you'd like to keep track of or add comments below.

<!-- DO NOT REMOVE -->
/confidential
/label ~"group::package" ~"section::ops" ~"devops::package" ~onboarding
/assign `ADD HERE MANAGER, ONBOARDING BUDDY AND TEAM MEMBER`
/due <!-- set a date 12 weeks after joining -->

<!-- Replace with the corresponding links -->
[package-onboarding-issue]: `INSERT LINK TO PACKAGE STAGE ONBOARDING ISSUE`

<!-- Instructions:
[ ] Name the issue title as "Member's onboarding to Container Registry"
[ ] Update at the top of the description the title with the new team member name
[ ] In the quick actions update users handlers and due date
[ ] Update links

-->
