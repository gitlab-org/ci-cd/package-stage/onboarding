## :package: `ADD FIRST NAME`, Welcome to the GitLab Package Registry!

It's great to have you in the Package team and working on the [Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/)!

This document is intended to be a reference point for you as you go through your technical engineering onboarding and start to ramp up over the first couple of months. There are a lot of links here to various issues and pages that will be useful as you get comfortable with the project. 

This is **your** issue, so feel free to make changes, add comments, or keep track of anything you want to review with your manager.

For general Package onboarding information, please refer to your [general onboarding issue][package-onboarding-issue].

## :radio: Point of contact

This issue being fairly large, it can be a source of confusion.

If you have any questions, don't hesitate to contact:

- Your onboarding buddy `ADD BUDDY HANDLE`
- The Package Engineering Manager `ADD MANAGER HANDLE`
- The `#s_package` stage and `#g_package-registry` group channels on Slack.

---
### Day 1

<details>
<summary>Manager Tasks</summary>

1. [ ] Add new team member to google and gitlab groups
      1. [ ] https://groups.google.com/a/gitlab.com/g/eng-dev-package-package-registry - Package Registry group
      1. [ ] https://gitlab.com/groups/gitlab-org/ci-cd/package-stage/package-registry-group/-/group_members - Package Registry group
1. [ ] Invite new team member to the slack channels
    1. [ ] #g_package-registry
    1. [ ] #g_package-registry_standups
1. [ ] Add new member to [Group standup](https://app.geekbot.com/dashboard/)
1. [ ] Add new member to monthly async retrospective [participants](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml) list and to the [project team](https://gitlab.com/gl-retrospectives/package-stage/package-registry/activity)

</details>

## :desktop: Features Presentation

- [ ] Before diving into features, make sure you read the [Package Team group page](https://about.gitlab.com/handbook/engineering/development/ops/package/). This is also listed in your general onboarding in week 3.

The Package Registry side of the Package team is responsible for 3 distinct parts:
  - [ ] [The Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/) (obviously).
    - [ ] Make sure to read through the [supported formats section](https://docs.gitlab.com/ee/user/packages/package_registry/#supported-package-managers). This will give you an overview of what is supported currently and what we plan to support in the future.
  - [ ] [The Dependency Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/).
  - [ ] [Some features and the UI around the Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/).


## :cruise_ship: Engineering onboarding

This technical onboarding will get you through:
1. Some reading to acquire minimal context knowledge around how we work.
1. Operational tasks to set up some tools and show you our dashboards.
1. Setting up a local environment.
1. Playing with our features using your local environment.
1. Your first actual contributions.

### :book: Documentation

- [ ] Read [GitLab architecture overview](https://docs.gitlab.com/ee/development/architecture.html).
  - We work mainly in the Rails backend (Puma) and sometimes in Workhorse.

Let's focus on the Rails backend for the next reads.

- [ ] Read [Reusing Abstractions](https://docs.gitlab.com/ee/development/reusing_abstractions.html).
- [ ] Read [Feature flags for GitLab development](https://docs.gitlab.com/ee/development/feature_flags/).
  - Feature flags are a powerful piece of logic that we extensively use to slowly deploy our most impactful changes.
- [ ] Have a look at [Database best practices](https://docs.gitlab.com/ee/development/database/#best-practices). 
  - Get a feeling for the database constraints we work with (example: we don't have downtime during migrations).

Lastly, let's focus on the Package Registry code area specifically with:

- [ ] Read the [Package Development Guide](https://docs.gitlab.com/ee/development/packages.html).
- [ ] Read the [File Uploads Developement documentation](https://docs.gitlab.com/ee/development/uploads.html).
  - We are heavy users of file uploads. Don't try to understand all the details but get the main overview which is: Workhorse handles file uploads for us.
- [ ] Read the [Sidekiq guides](https://docs.gitlab.com/ee/development/sidekiq/).
  - We are also heavy users of background jobs. Given the complexity, we use a few custom options on top of standard Sidekiq jobs. Again, don't try to get every detail. 
- [ ] Have a look at the 2 core model files for the Package Registry: the [package model](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/packages/package.rb) and the [package file model](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/packages/package_file.rb). 
  - The idea is not to grasp all the details but to have an early look at the models and how they are designed. Example: notice that we have a few validators for the `name` and the `version` fields of the `Packages::Package` model.

Now that you know more about the Package Registry and its architecture let's have a look at our code review guidelines.
Any of your contributions will need a review, so it's better to have a good understanding:

- [ ] Read the [Code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
  - [ ] Pay attention to the [The responsibility of the merge request author](https://docs.gitlab.com/ee/development/code_review.html#the-responsibility-of-the-merge-request-author) section.
- [ ] Make sure you read [Database Review Guidelines](https://docs.gitlab.com/ee/development/database_review.html).


### :construction_site:  Operational tasks

- [ ] Familiarize yourself with [metrics and dashboards](https://about.gitlab.com/handbook/engineering/development/ops/package/#dashboards) specifically the [Package dashboard](https://dashboards.gitlab.net/d/stage-groups-package/stage-groups-package-group-dashboard?orgId=1).
- [ ] Explore our [activity dashboard](https://app.periscopedata.com/app/gitlab/527857/Package-GitLab.com-Stage-Activity-Dashboard).
- [ ] Make sure you can access the [production logs](https://log.gprd.gitlab.net/).
- [ ] Make sure you can access the [gitlabcom project on Sentry](https://sentry.gitlab.net/gitlab/gitlabcom/).
- [ ] Explore [the `Packages` Group](https://gitlab.com/issue-reproduce/packages) we created on GitLab.com with several different package formats.
  - This is a good time to explore the UI that presents the Packages information.
- [ ] Login to https://gitlabsandbox.cloud/ using Okta. Go to https://gitlabsandbox.cloud/account/profile and get your profile ID. Post a comment in this issue tagging your manager and onboarding buddy so they can add you to the project.
    - [Sandbox Cloud](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox) is a platform to provision environments on GCP/AWS. We have a GCP project for the team within our sandbox cloud group. The idea is for everyone to share this GCP project for dev/test/demo purposes when needed.


### :gear: Local Developement Environment

Enough of our readings for now. Time to get our hands on the code.

You're free to set up your local installation as you want, but it is strongly recommended to use the [GitLab Development Kit (or GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit). 
If you recall the [GitLab architecture overview](https://docs.gitlab.com/ee/development/architecture.html), we can notice that GitLab is made up of many, many parts.

Installing and configuring each part one by one is possible _but_ that's exactly the goal of GDK: it will install, configure and maintain all the parts for you. There is even a [one-line installation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation).

Without further ado, let's:
- [ ] [Install GDK](https://gitlab.com/gitlab-org/gitlab-development-kit#installation).

Once GDK is installed, set up the additional parts that we use in the Package Registry. 

`(Optional)` denotes an optional component. 
You don't need it right at the beginning of your journey, but sooner or later, you will work on that part, and so you will need to set it up if it hasn't been done during this onboarding.

- [ ] [Object Storage](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/object_storage.md).
- [ ] [Container Registry](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/registry.md).
- [ ] (Optional) [Dependency Proxy](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/dependency_proxy.md).
- [ ] (Optional) [Runner](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/runner.md).

In the `gitlab` repository (the rails backend), let's
- [ ] Setup the [pre-push static analysis git hook](https://docs.gitlab.com/ee/development/contributing/style_guides.html#pre-push-static-analysis-with-lefthook).
  - GitLab rails come with a quite large set of rubocop/linter rules. Some of them can be executed _before_ pushing commits.
  - Bonus points: set up your code editor to have rubocop rules executed on file save and mark offenses in the code.


### :popcorn: Let's play around

Now that we have everything set up let's play around with our local GDK instance.

Package Registry:
- [ ] You will need a Project for this part. [Create one project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project) in your local instance.
- [ ] [Create](https://docs.gitlab.com/ee/user/packages/npm_registry/#create-a-project) an NPM package and [publish](https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package) it in your local GDK instance.
- [ ] Open the Project on your local GDK instance and browse the Package Registry. Your shiny new package should be listed there. Click on it to see more details.
- [ ] Bonus points: explore the NPM API endpoints and their related classes.
- [ ] Bonus points: Publish a [package from a CI pipeline](https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package-by-using-cicd).
   - You will need to have set up a [Runner](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/runner.md) locally.
  
Container Registry:
- [ ] You will need a Project for this part. [Create one project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project) in your local instance.
- [ ] [Buid a new container image and publish it](https://docs.gitlab.com/ee/user/packages/container_registry/#build-and-push-images-by-using-docker-commands) in your local GDK instance.
- [ ] Open the Project on your local GDK instance and browse the Container Registry. Your brand new container image should be listed there. Click on it to see more details.
- [ ] Bonus points: Explore the `ContainerRepository` model.

Dependency Proxy (if you set up this component during the GDK installation):
- [ ] You will need a Group for this part. [Create one group](https://docs.gitlab.com/ee/user/group/manage#create-a-group) in your local instance.
- [ ] [Manually pull](https://docs.gitlab.com/ee/user/packages/dependency_proxy/#use-the-dependency-proxy-for-docker-images) a container image (eg. `alpine:latest`) from DockerHub through the dependency proxy.
- [ ] [Check the Dependency Proxy Storage Use](https://docs.gitlab.com/ee/user/packages/dependency_proxy/reduce_dependency_proxy_storage.html#check-dependency-proxy-storage-use) as of now, there will be some entries in the cache.
- [ ] Bonus points: Explore the `Groups::DependencyProxyForContainersController` controller.

At this point, feel free to explore our code area in the Rails backend code source.

## :hammer_pick: Time to contribute!

Did you make this far? Congratulations! :checkered_flag: 

You now have all the basics to start contributing!

Here are a couple of tasks that we’ve found that you can take on. These should be fairly straightforward, but if they turn out to be too complex, let us know. These first tasks aren’t intended to be complex features, they are intended to be simple tasks that will help you get familiar with the process of shipping code at GitLab.

TBD: `ADD BUDDY HANDLE` please add 2 to 3 getting started issues and include in `Milestone of week 4`.

1. [ ] xyz
1. [ ] xyz
1. [ ] xyz

## :notepad_spiral: Additional notes

After completing your onboarding and getting started tasks, you should feel comfortable to start normally contributing to the teams' milestone efforts.
Feel free to reach out to your manager or peers for guidance if you don't know how/what to do.

- [ ] Review the current milestone's planning issue and pick an issue to start working on it!
- [ ] Close this issue :tada:

Congratulations and welcome to the Package registry!

<!-- DO NOT REMOVE -->
/confidential
/label ~"group::package registry" ~"section::ops" ~"devops::package" ~onboarding
/assign `ADD MANAGER, BUDDY AND TEAM MEMBER HANDLE`

[package-onboarding-issue]: `ADD LINK`

<!-- Instructions:
[ ] Update title with the new team member's name
[ ] In the quick actions, update the manager, buddy, and team member handles
[ ] Update the list of links above
