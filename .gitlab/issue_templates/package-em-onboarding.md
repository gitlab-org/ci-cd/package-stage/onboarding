## ---, Welcome to GitLab!

We’re all excited you’ll be joining the Package group. This document is intended to be a reference point for you as you go through your onboarding and start to ramp up over the first couple months. Consider this document a reference. There are a lot of links here to various issues and pages in the Handbook that will be useful as you get comfortable with how things work at GitLab. This is **your** issue, so feel free to make changes, add comments, or keep track of anything you want to review with your manager.

---
### Your Onboarding Issue
This is where you’ll get started. It contains the better part of a week’s worth of onboarding tasks. There’s a lot to do in here, so be sure to pace yourself. It’s broken down by day to make it easier to digest.

<- Insert Link Here ->

---
### Package Team

* [About the team](https://about.gitlab.com/handbook/engineering/development/ops/package/)
* Setup [Coffee Chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) with everyone on the team
* [Product Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3A%3Apackage)
* Current Release %"14.2" - [Package Group 14.1 Milestone Plan](https://gitlab.com/gitlab-org/gitlab/-/issues/331743)

### Helpful Reading Material
* [Package Strategy and Direction](https://about.gitlab.com/direction/package/): This page is updated monthly and details our vision as well as details about what we will work on and why.
* [Team youtube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0KoPiSySNHTfvxC20i0LppMf): This is where we post all of our team meetings and discussions.
* [User Interviews](https://www.youtube.com/watch?v=sBjimcXtyC4&list=PL05JrBw4t0KpxCv3B5S-6LFCpBB6NCnga): This is a private youtube channel where we host all of our user interviews. Please note, these should not be shared outside of GitLab. If you get a "Video not available" page, make sure that you [accepted the invitation](https://about.gitlab.com/handbook/communication/youtube/#access) in your google account. If you still get the error page, make sure that on youtube, you switched to the `GitLab Unfiltered` account.
* [Product Foundations](https://docs.google.com/document/d/1mklxWVL4FQmAzsG1Nx6PT4wNwI-FpEpzVR2enT9wwu8/edit#): This is a summary of our stage, it's use cases and values.
* [Package Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1196366?label_name[]=devops%3A%3Apackage): This is our main planning board and it details the current and upcoming milestones and issues we are working towards.
* [Package Bug Board](https://gitlab.com/groups/gitlab-org/-/boards/1200744?label_name[]=bug&label_name[]=devops%3A%3Apackage): This is our bug board. It's intended to be the single source of truth of open bugs for our stage.
* [Assignments Board](https://gitlab.com/groups/gitlab-org/-/boards/1200765?label_name[]=group%3A%3Apackage): This is a board view of who is working on what on the team.
* [Important Dates at GitLab](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline)

---

### Package Onboarding Tasks

#### Before First Week

* [ ] Manager: Add to weekly team meetings, and monthly get together call
* [ ] Manager: Add to retro participants list
* [ ] Manager: Invite new team member to the following slack channels: #s_package, #backend or #frontend, #celebrations, #development, #thanks, #eng-week-in-review, #s_package_standups
* [ ] Manager: Assign onboarding buddy

#### First Week 

- [ ] Schedule weekly 1:1s with all the direct reports members, these may need some adjustments due to different time zones
- [ ] Schedule weekly 1:1 with the PM
- [ ] Schedule bi-weekly 1:1 with the team stable counterparts

#### First 30 days

- [ ] Ensure the access to lead-honestly is correctly setup and all the direct reports are shown. 
- [ ] Familiarize yourself with all the 'news outlet' of the company: Slack channels, GitLab unfiltered, Managers meetings
- [ ] Review and be confident with the milestone planning workflow: https://about.gitlab.com/handbook/engineering/development/ops/package/#milestone-planning
- [ ] Optional: Ensure your GDK is running and the Package Stage features are working
- [ ] Optional: read this comment regarding PM expectations: https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/15#note_504383974

#### First 60 days

- [ ] Be sure to have access to all the [reporting dashboards](https://about.gitlab.com/handbook/engineering/development/ops/package/#dashboards)
  - [ ] Grafana: https://dashboards.gitlab.net/d/stage-groups-package/stage-groups-group-dashboard-package-package?query=package&search=open&folder=current&orgId=1
  - [ ] [Package Dashboard](https://dashboards.gitlab.net/d/stage-groups-package/stage-groups-group-dashboard-package-package) (Grafana)
  - [ ] [Container Registry](https://log.gprd.gitlab.net/goto/e7b62a23a5a9cdc88aa1de3cdb392758) (Kibana)
  - [ ] [Container Registry garbage collection details](https://dashboards.gitlab.net/d/registry-gc/registry-garbage-collection-detail?orgId=1&from=now-30d&to=now&var-PROMETHEUS_DS=Global&var-environment=pre&var-cluster=pre-gitlab-gke&var-stage=main&var-namespace=gitlab&var-Deployment=gitlab-registry) (Grafana)
  - [ ] [Dependency Proxy](https://log.gprd.gitlab.net/goto/3d363d13eaf8133a4216149e335ab1b9) (Kibana)
  - [ ] [Cleanup policies for Container Images](https://log.gprd.gitlab.net/goto/b03da1dcaf2ac281994f72687347f40a) (Kibana)
  - [ ] [GitLab.com activity dashboard](https://app.periscopedata.com/app/gitlab/527857/Package-GitLab.com-Stage-Activity-Dashboard) (SiSense)
  - [ ] [Package usage funnel](https://app.periscopedata.com/app/gitlab/854785/Package-Usage-Funnel) (SiSense)
  - [ ] [Package user adoption and growth](https://app.periscopedata.com/app/gitlab/805350/Package:-User-Adoption-and-Growth) (SiSense)
  - [ ] [Package customer adoption](https://app.periscopedata.com/app/gitlab/877343/Package-customer-adoption) (SiSense)
- [ ] Have a handle of direct report career planning documents and setup meetings to progress on them
- [ ] Remember to review lead-honestly pulse score
- [ ] Complete the Interviewer training

#### First 90 days

- [ ] Familiarize yourself and keep track with the team OKRs
- [ ] Keep an eye on team PTOs and encourage people to take time off when appropriate
- [ ] Review the team tech-debt status and discuss with the PM scheduling some tech-debt issues if necessary
- [ ] Consider scheduling some PTO time for yourself, remember we are Family & Friends first.
