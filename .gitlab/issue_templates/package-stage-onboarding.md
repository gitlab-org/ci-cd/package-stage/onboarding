## `ADD FIRST NAME`, Welcome to 📦 Package Stage! 🥳

We're all excited you'll be joining the [Package stage](https://about.gitlab.com/handbook/engineering/development/ops/package/). This issue is intended to be a reference point for you as you go through your onboarding and ramp up over the first couple of months. We erred on the side of massively overloading you with information - don't worry, you'll have time to walk through all of this.

⚠️ Getting started here can feel pretty overwhelming - lots of new people, lots of documentation, IT setup. We'd suggest just diving in and embracing the fact that you will be disoriented for a while. 
Don't worry if you're feeling totally confused and lost at the beginning - it happens to everyone! We're all here to help; please don't hesitate to ask questions.

Consider this document a reference. We've included lots of information and links that will be useful as you get comfortable with how things work at GitLab.
If you are an existing team member transferring from another team, consider some of these topics as refreshers. They are probably already known to you,
but this may be an excellent time to go over them again.

This is **your** issue, so feel free to make changes, add comments, or keep track of anything you want to review with your manager.

---

### Support

<!-- Replace aa and bb with the manager and onboarding buddies handlers -->
If you cannot find the information you are looking for, please be sure to reach out to your Manager (`ADD MANAGER HANDLE`) or Onboarding Buddy (`ADD BUDDY HANDLE`) for support by tagging them in the comments section of this issue.  
If you need to troubleshoot specific issues with tools, you can directly add a screenshot of the issue in the comment to help them diagnose the problem.


### Your onboarding issues

For better organization, all the onboarding issues related to Package (those without the star) are [linked](https://docs.gitlab.com/ee/user/project/issues/related_issues.html#linked-issues) to this issue.

- [GitLab general onboarding][general-onboarding-issue] - Everything about joining GitLab
- Package stage onboarding (this issue) - How the stage works and what the stage does
- [Package Registry group onboarding][package-group-onboarding-issue] - Specific to the group you are joining, mostly the technical onboarding <!-- mark as optional if the new member will not work with this category -->
- [Container Registry group onboarding][container-group-onboarding-issue] - Specific to the group you are joining, mostly the technical onboarding <!-- mark as optional if the new member will not work with this category -->
- [Interviewer training][interviewer-issue] <!-- remove if the new team member will not have this training -->


## 30-60-90 days onboarding plan

We understand onboarding is an overwhelming process. We want to ease the process and came up with a 30-60-90 days plan to give you an idea of how you should spend your first days at GitLab and the Package stage.

🎯 After 90 days at GitLab, you should:
- Have a degree of fluency in GitLab's processes and [Package's processes](https://about.gitlab.com/handbook/engineering/development/ops/package/#how-we-work) specific to your role and feel a medium level of comfort with the code and/or the Package domain.
- Be delivering solutions or features of simple and straightforward complexity.
- Have a high-level foundation of knowledge around [Package categories](https://about.gitlab.com/stages-devops-lifecycle/package/) and features.

⚠️ We understand that covering all the material outlined in your onboarding issues and working towards your Package stage goals right from week one can be overwhelming, therefore, this issue **starts on week 2**. To be successful in your onboarding, at GitLab, we want team members to be [manager of one](https://about.gitlab.com/handbook/leadership/#managers-of-one) and develop their daily priorities to achieve goals.

There's a lot to do in here, so **be sure to pace yourself**. It's broken down by week to make it easier to digest.

| Milestone | Week | Area of Focus |
| ----- | ----- | ----- |
| **30 days** | 2-4 | **Learn** |
| | Week 2 | Learn about Package stage and its categories |
| | Week 3 | Understand how Package stage works |
| | Week 4 | Set up your working environment |
| **60 days** | 5-9 | **Implement** and complete your first issue |
| | Week 5 | Learn by doing. Use Package categories and features |
| **90 days** | 10-12 | **Optimize** |


---

### Before starting

<details>
<summary>Manager Tasks</summary>

1. [ ] Select an [onboarding buddy](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/) and assign them to this issue
1. [ ] Create an [specialty onboarding issue](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/new) (Package or Container) and [link it](https://docs.gitlab.com/ee/user/project/issues/related_issues.html) to this issue

</details>

### Day 1

<details>
<summary>Manager Tasks</summary>

1. [ ] Add new team member to google and gitlab groups
    1. [ ] https://groups.google.com/a/gitlab.com/g/eng-dev-package-package - Package stage
    1. [ ] https://gitlab.com/gitlab-org/ci-cd/package-stage
    1. [ ] https://gitlab.com/groups/gitlab-org/ci-cd/package-stage
1. [ ] Invite new team member to the slack channels
    1. [ ] #s_package
    1. [ ] #s_package_social
    1. [ ] #g_cicd_social
1. [ ] Add new member to [Package Weekly Social](https://app.geekbot.com/dashboard/)
1. [ ] Add new member to [CI/CD daily social](https://app.geekbot.com/dashboard/)
1. [ ] Create a [slack emoji](https://slack.com/help/articles/206870177-Add-custom-emoji-and-aliases-to-your-workspace) for the new member and share with them and the relevant channels. Use their slack profile picture and remove the background with online tool https://www.remove.bg/
1. [ ] Introduce new team members in relevant Slack channels
1. [ ] [Access Requests](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/)   
    1. [ ] [1Password QA vault](https://about.gitlab.com/handbook/engineering/development/ops/package/quality#how-can-i-have-access-to-the-gitlab-qa-vault-in-1password) 
    1. [ ] Dovetail view access ([example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/17191))
1. [ ] Add member to [Lead Honestly](https://leadhonestly.com/team) team and schedule weekly 1:1
1. [ ] Add member as co-host on Zoom team meetings. Make sure to do it for EMEA and APAC
    1. [ ] weekly sync
    1. [ ] weekly retrospective
1. [ ] Add a list for the new member in the [Assigments dashboard](https://gitlab.com/groups/gitlab-org/-/boards/1200765)

</details>

### Week 2: Learn about the Package stage and its categories

<details>
<summary>Buddy Tasks</summary>

1. [ ] Schedule an introduction call with the new team member. Make sure to walk through the new team member's calendar and ensure that the team meeting and other meetings are visible.
1. [ ] Schedule at least one follow-up chat on their second week and third week
1. [ ] Work with your engineering manager and product manager to identify 2 to 3 easy issues that the new team member should tackle
    1. [ ] List the issues in the [group specialty issue][getting-started-issues]
    1. [ ] Schedule the issues in the milestone that falls in the week 4 of the onboarding
</details>


</details>

New member Tasks

:computer: **Do:**
1. [ ] Add yourself to the team's timezone.io account, here: [https://timezone.io/join/2eb63512b0861b59](https://timezone.io/join/2eb63512b0861b59)
1. [ ] Attend sync team meetings or read the notes for those meetings you cannot attend synchronously. _Note: The link to the notes document is in the calendar event_
1. [ ] Link PTO by Roots to Package Stage Calendar. Go to PTO by Roots in Slack -> Home -> Calendar Sync in dropdown -> Additional calendars to include?.  Add Package Shared Calendar ID `c_5kbg522u4n99le6j6j1eejg2a4@group.calendar.google.com`
1. [ ] Join different slack channels, like
    1. [ ] #ci-section
    1. [ ] #development
    1. [ ] #engineering-fyi
    1. [ ] #celebrations
    1. [ ] #thanks

:book: **Learn:**
1. [ ] Learn the difference between [stages and groups](https://about.gitlab.com/handbook/product/categories/#hierarchy). Understand that Package is a stage and also a group. 
1. [ ] Take a look at the [Package and its categories summary](https://about.gitlab.com/stages-devops-lifecycle/package/)
1. [ ] Learn more about the roadmap in the Package [direction page](https://about.gitlab.com/direction/package/) and reach out to our Product Manager with any questions.
1. [ ] Watch [demos and speed runs](https://about.gitlab.com/handbook/engineering/development/ops/package/#demos--speedruns) to learn more about Package categories. **NOTE** See [Product's definition of demos and speed runs](https://about.gitlab.com/handbook/product/product-processes/#speed-run).
    1. [ ] [Videos](https://about.gitlab.com/handbook/engineering/development/ops/package/#package-registry) for Package registry (aprox 2hrs)
    1. [ ] [Videos](https://about.gitlab.com/handbook/engineering/development/ops/package/#container-registry) for Container Registry (aprox 20mins)
    1. [ ] [Videos](https://about.gitlab.com/handbook/engineering/development/ops/package/#dependency-proxy) for Dependency Proxy (aprox 20mins)
    1. [ ] [Videos](https://about.gitlab.com/handbook/engineering/development/ops/package/#dependency-firewall) for Dependency Firewall (aprox 15mins)
1. [ ] Read the [Package Positioning Deck](https://docs.google.com/presentation/d/1UQIwVk4qwQFR39FW5iL5oKsF9PaJer1c2GfEPXNbhwQ/edit#slide=id.ge58c2fe17f_0_144) and watch the [recorded walk through](https://www.youtube.com/watch?v=PEe0VgOYbqY) to understand the use cases and values of our stage.
1. [ ] Watch the [video overview](https://youtu.be/gsSWEqX4dOA) of the Package roadmap.

:handshake: **Meet:**
1. [ ] Set up [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) between this and the following weeks to meet [your team](https://about.gitlab.com/company/team/?department=package-group) 
1. [ ] Have a call with your manager
1. [ ] Have a call with your onboarding buddy


### Week 3: Understand how the Package stage works

<details>
<summary>Buddy Tasks</summary>

1. [ ] Check in with the new team member and see if they need help 

</details>


New member Tasks

:computer: **Do:**
1. [ ] Participate in team meetings
1. [ ] Participate in async standups by sharing yours and engaging with the rest of the team. We use [Geekbot](https://gitlab.slack.com/apps/A0H67RAG0-standup-bot-by-geekbot?tab=more_info) and [here](https://about.gitlab.com/handbook/engineering/development/ops/package/#async-daily-standups) we describe how we use it.
1. [ ] (Optional) Consider organizing your email using filters and labels. Watch this [video](https://youtu.be/YOgm-vZVqng) to learn how


:book: **Learn:**
1. [ ] Learn about [Package stage](https://about.gitlab.com/handbook/product/categories/#package-stage) with special attention to our slack channel and handler, people in the team, stable counterparts, and our focus 
1. [ ] Get familiar with [Package stage](https://about.gitlab.com/handbook/engineering/development/ops/package/) and pay special attention to [how we work](https://about.gitlab.com/handbook/engineering/development/ops/package/#how-we-work) and [recurring meetings](https://about.gitlab.com/handbook/engineering/development/ops/package/#recurring-meetings)
1. [ ] Watch the most recent Package Stage Kickoff calls:
  - [ ] [Package:Package Registry group playlist][https://www.youtube.com/playlist?list=PL05JrBw4t0KoVP8cJft-Hv4kQH__aFaGS]
  - [ ] [Package:Container Registry group playlist][https://www.youtube.com/playlist?list=PL05JrBw4t0Krw58pTducgwZ80phVEOzpG]
1. [ ] Take a look at the Package [milestone plan][https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Planning%20Issue&label_name%5B%5D=devops%3A%3Apackage&milestone_title=Upcoming] for the current release. Other milestone plans can be found in the [Milestone planning epic](https://gitlab.com/groups/gitlab-org/-/epics/3591)
1. [ ] Read about how Gitlab uses labels [Issue Workflow Labels](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/development/contributing/issue_workflow.md), [Labels FOSS](https://gitlab.com/gitlab-org/gitlab-foss/-/labels)
1. [ ] Get familiar with the [GitLab development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) and important dates
1. [ ] Familiarize yourself with the team boards
    1. [ ] [Package Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1196366?label_name[]=devops%3A%3Apackage): This is our main planning board, and it details the current and upcoming milestones and issues we are working towards.
    1. [ ] [Package Bugs Board](https://gitlab.com/groups/gitlab-org/-/boards/1200744?label_name[]=devops%3A%3Apackage&label_name[]=type%3A%3Abug): This is our bugs board. It's intended to be the single source of truth (SSOT) of open bugs for our stage.
    1. [ ] [Assignments Board](https://gitlab.com/groups/gitlab-org/-/boards/1200765): This is a board view of who is working on what on the team.
    1. [ ] [Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1284221): This is our board to see at which stage of the development workflow are all our issues. 
1. [ ] Learn about the different projects we work in, depending on what type of work. For all projects, keep in mind the [SAFE framework](https://about.gitlab.com/handbook/legal/safe-framework/) for sharing information.
    1. [ ] [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name%5B%5D=group%3A%3Apackage): Work on GitLab, the product.
    1. [ ] [gitlab-com/www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues?scope=all&state=opened&label_name%5B%5D=group%3A%3Apackage): Work on the handbook or blog.
    1. [ ] [package-stage/package](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues): Work on team organization, team styles, and how we work. 
    1. [ ] [package-combined-team/team](https://gitlab.com/package-combined-team/team/-/issues): Things that do not fit in the above projects or things that we want to keep to just Package team members.
1. [ ] Start to familiarize yourself with the documentation for the group or specialty you will be working by reading some of the material from your [specialty onboarding issue][specialty-onboarding] 

:handshake: **Meet:**
1. [ ] (optional) Share about you and [get to know the team](https://gitlab.com/package-combined-team/team/-/issues/57)
1. [ ] Have a [coffee chat](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) with our product manager.
1. [ ] Schedule [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to meet our [stable counterparts](https://about.gitlab.com/handbook/engineering/development/ops/package/#stable-counterparts) anytime between this and the following weeks



### Week 4: Set up your working environment

<details>
<summary>Onboarding buddy Tasks</summary>

1. [ ] Work closely with the new team member as they work on their [development setup](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main/doc#install-and-configure-gdk)
1. [ ] Do an intro session about the GDK and specifically about our FE Tools (for example, the best way to run Tests, etc.).

</details>

New member Tasks

:computer: **Do:**
1. [ ] Complete the engineering division tasks listed in your [GitLab general onboarding issue][general-onboarding-issue]
1. [ ] Follow the developer onboarding to set up your work environment. Your [specialty onboarding issue][specialty-onboarding] also covers some important steps to complete your local environment setup specifically for Package features.

:book: **Learn:**
1. [ ] Familiarize yourself with the [engineering handbook](https://about.gitlab.com/handbook/engineering) and relevant pages linked from there.
1. [ ] Learn about the [product development workflow](https://about.gitlab.com/handbook/product-development-flow/). Specifically the [workflow for build](https://about.gitlab.com/handbook/product-development-flow/#build-track) 
1. [ ] Learn about how we use `workflow` labels during [build phase](https://about.gitlab.com/handbook/product-development-flow/#build-phase-2-develop--test) and what the [develop and test phase](https://about.gitlab.com/handbook/product-development-flow/#description-5) is
1. [ ] Learn about the [Merge request workflow](https://docs.gitlab.com/ee/development/code_review.html)
1. [ ] Familiarize yourself with the documentation for the group or specialty you will be working by completing the learning and reading material from your [specialty onboarding issue][specialty-onboarding] 

:handshake: **Meet:**
1. [ ] By the end of the week, you ideally met all your peers

### Week 5: Learn by doing. Use Package categories and features

<details>
<summary>Buddy Tasks</summary>

1. [ ] Show one of your current deliverables and explain based on that: What you have actually done, How did you find the actual relevant files, reproducing steps, How our process works, How to submit an MR, When to involve reviewers, etc.

</details>

New member Tasks

:computer: **Do:**

Note: _Some of the following tasks may be duplicated in your group specialty onboarding issue._

1. [ ] Complete the operational tasks from your [specialty onboarding issue][specialty-onboarding]
1. [ ] Pick a package type (you don't have to be familiar with the format). Create and upload a package to the package registry (you can create a project on GitLab.com or use GDK if you have it set up).
1. [ ] Create an image and try to push and pull it from the container registry (you can create a project on GitLab.com or use GDK if you have it set up).
1. [ ] Log into the dependency proxy and pull an image through a group you belong to. You can use the [subgroup this issue is in](https://gitlab.com/groups/gitlab-org/ci-cd/package-stage/-/dependency_proxy). Doing this is also possible within GDK, but additional [configuration](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/dependency_proxy.md) is required.
1. [ ] Try to do one of the above exercises within a CI job.
1. [ ] Try to use any of the [ad-hoc test projects](https://gitlab.com/gitlab-org/ci-cd/package-stage/ad-hoc-test-projects) to test and use the package registry's capabilities

:book: **Learn:**
1. [ ] Read the [getting started issues][getting-started-issues] for your role. This information will be valuable for you to tailor the rest of your onboarding according to what you will need to deliver those issues
1. [ ] Read David's advice for [balancing learning vs implementing](https://gitlab.com/gitlab-org/gitlab/-/issues/330475#note_695799964). This can be valuable now that you start to take issues to implement, but you are still in a continuous learning process.

:handshake: **Meet:**
1. [ ] By the end of the week, you ideally met all our stable counterparts



### Weeks 6-9: Implement

<details>
<summary>Manager Tasks</summary>

1. [ ] On week 6, do a check in with the new team member. Ask about their experience, help them navigate any concerns, and highlight the results. If applicable, talk about the probation period and their performance. 

</details>

<details>
<summary>Buddy Tasks</summary>

1. [ ] Check in how far the GitLab team-member has gotten in their onboarding
1. [ ] Ask where they need help and connect them with the experts

</details>

New member Tasks

:computer: **Do:**
1. [ ] Collaborate with your team by doing [code reviews](https://about.gitlab.com/handbook/engineering/workflow/code-review/)
1. [ ] Work and deliver one of the [getting started issues][getting-started-issues] for your role

:book: **Learn:**
1. [ ] Familiarize with the [testing best practices](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html)
1. [ ] Get familiar with the [GitLab Quality Assurance End-to-End Testing](https://about.gitlab.com/handbook/engineering/development/ops/package/quality/) for our group
1. [ ] Learn what are [feature flags](https://docs.gitlab.com/ee/development/feature_flags) and how we use them to [roll out changes](https://docs.gitlab.com/ee/development/feature_flags/controls#rolling-out-changes)


:handshake: **Meet:**
1. [ ] Around week 6, have a check in with your manager to evaluate your progress on this onboarding


### Weeks 10-12: Optimize

<details>
<summary>Manager Tasks</summary>

1. [ ] On week 10, do a check in with the new team member. Ask about their experience, help them to navigate any concerns and highlight the results. If applicable, talk about the probation period and their performance.

</details>

<details>
<summary>Buddy Tasks</summary>

1. [ ] Check in how far the GitLab team-member has gotten in their onboarding
1. [ ] Ask where they need help and connect them with the experts
1. [ ] Ask if they need help to propose improvements

</details>

New member Tasks

By this time, you are grounded in your role and probably identified gaps and areas of improvement. 
During these four weeks, you'll propose and implement changes to reduce those gaps.
The improvements could be either for the Package stage or for your work style and how to adapt to GitLab and the team.

:computer: **Do:**
1. [ ] Continue working on the [getting started issues][getting-started-issues] for your role
1. [ ] Propose an improvement to the package handbook, documentation, or onboarding templates
1. [ ] If you feel comfortable, please write an article or create a video sharing your Package onboarding experience

:book: **Learn:**
1. [ ] Take some time to go over the remaining content listed in the Package [useful links](https://about.gitlab.com/handbook/engineering/development/ops/package/#-other-useful-links)

:handshake: **Meet:**
1. [ ] Schedule a [skip level](https://about.gitlab.com/handbook/leadership/skip-levels/) call with the manager of your manager
1. [ ] On week 12, have a check in with your manager to evaluate your success in the onboarding



### Tips and tricks:

This section shows a few tips and tricks that other team members have found useful before.

- [Tips for returning from PTO](https://about.gitlab.com/handbook/engineering/development/ops/package/#returning-from-pto)
- [Searching on GitLab.com](https://gitlab.com/gitlab-com/people-group/people-operations/General/-/issues/1274)
- [Searching the GitLab handbook like a pro](https://about.gitlab.com/handbook/tools-and-tips/searching/)
- [General tools and tips for working at GitLab](https://about.gitlab.com/handbook/tools-and-tips/)
- [10 tips to make you a productive GitLab user](https://about.gitlab.com/blog/2021/02/18/improve-your-gitlab-productivity-with-these-10-tips/)
- [Epic](https://gitlab.com/groups/gitlab-org/ci-cd/package-stage/-/epics/12) with more tips and tricks shared by Package team members

### Additional Notes

Feel free to **edit this issue** and add any notes you'd like to keep track of.
You can also use the threads below or add new comments.


<!-- DO NOT REMOVE -->
/confidential
/label ~"group::package" ~"section::ops" ~"devops::package" ~onboarding
/assign <!-- Include here the manager, buddy and team member handles -->
/due <!-- set a date 12 weeks after joining -->

<!-- Replace xxx with the links corresponding for each case -->
[general-onboarding-issue]: xxx
[interviewer-issue]: xxx
[package-group-onboarding-issue]: xxx
[container-group-onboarding-issue]: xxx
[specialty-onboarding]: xxx
[getting-started-issues]: xxx

<!-- Instructions:
[ ] Name the issue title as "Member's onboarding to Package stage"
[ ] Update at the top of the description the title with the new team member name
[ ] In the section "Support", include the handlers of the manager and the onboarding buddy 
[ ] In the section "Onboarding issues", remove issues or mark them as optional 
[ ] Update the links in the section above
[ ] In the quick actions, change the assignee to the new team member and the buddy and include the due date 12 weeks after joining
-->

