## :package: `First name`, Welcome to the GitLab Package Registry!

It's great to have you in the Package team and working on the [Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/)!

This document is intended to be a reference point for you as you go through setting up your environment and start contributing to our product. There are a lot of links here to various issues and pages that will be useful as you get comfortable with the project. 

This is **your** issue, so feel free to make changes, add comments, or keep track of anything you want to review with your manager.


## :radio: Point of contact

If you have any questions, don't hesitate to contact:

- The Package Engineering Manager `add the tag of the manager`
- The `add a channel here` channel on Slack
- The backend engineers `add the tags of the engineers` 

## :desktop: Features Presentation

- [ ] [The Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/)

## :cruise_ship: Engineering onboarding

This technical onboarding will get you through:
1. Some reading to acquire the minimal context knowledge around how we work.
1. Setting up a local environment.
1. Playing with our features using your local environment.

### :book: Documentation

- [ ] Read [GitLab architecture overview](https://docs.gitlab.com/ee/development/architecture.html).
  - We work mainly in the Rails backend (Puma) and sometimes in Workhorse.

Let's focus on the Rails backend for the next reads.

- [ ] Read [Reusing Abstractions](https://docs.gitlab.com/ee/development/reusing_abstractions.html).
- [ ] Read [Feature flags for GitLab development](https://docs.gitlab.com/ee/development/feature_flags/).
  - Feature flags are a powerful piece of logic that we extensively use to slowly deploy our most impactful changes.
- [ ] Have a look at [Database best practices](https://docs.gitlab.com/ee/development/database/#best-practices). 
  - Get a feeling for the database constraints we work with (example: we don't have downtime during migrations).

Lastly, let's focus on the Package Registry code area specifically with:

- [ ] Read the [Package Development Guide](https://docs.gitlab.com/ee/development/packages.html).
- [ ] Read the [File Uploads Developement documentation](https://docs.gitlab.com/ee/development/uploads.html).
  - We are heavy users of file uploads. Don't try to understand all the details but get the main overview which is: Workhorse handles file uploads for us.
- [ ] Read the [Sidekiq guides](https://docs.gitlab.com/ee/development/sidekiq/).
  - We are also heavy users of background jobs. Given the complexity we use a few custom options on top of standard Sidekiq jobs. Again, don't try to get every detail. 
- [ ] Have a look at the 2 core model files for the Package Registry: the [package model](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/packages/package.rb) and the [package file model](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/packages/package_file.rb). 
  - The idea is not to grasp all the details but to have an early look at the models and how they are designed. Example: notice that we have a few validators for the `name` and the `version` fields of the `Packages::Package` model.

Now that you know more about the Package Registry and its architecture, let's have a look at our code review guidelines.
Any of your contributions will need a review, so it's better to have a good understanding:

- [ ] Read the [Code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).
  - [ ] Pay attention to the [The responsibility of the merge request author](https://docs.gitlab.com/ee/development/code_review.html#the-responsibility-of-the-merge-request-author) section.
- [ ] Make sure you read [Database Review Guidelines](https://docs.gitlab.com/ee/development/database_review.html).


### :construction_site:  Operational tasks

- [ ] Familiarize yourself with [metrics and dashboards](https://about.gitlab.com/handbook/engineering/development/ops/package/#dashboards) specifically the [Package dashboard](https://dashboards.gitlab.net/d/stage-groups-package/stage-groups-package-group-dashboard?orgId=1).
- [ ] Explore our [activity dashboard](https://app.periscopedata.com/app/gitlab/527857/Package-GitLab.com-Stage-Activity-Dashboard).
- [ ] Make sure you can access the [production logs](https://log.gprd.gitlab.net/).
- [ ] Make sure you can access the [gitlabcom project on Sentry](https://sentry.gitlab.net/gitlab/gitlabcom/).
- [ ] Explore [the `Packages` Group](https://gitlab.com/issue-reproduce/packages) we created on GitLab.com with several different package formats.
  - This is a good time to explore the UI that present the packages information.


### :gear: Local Developement Environment

Enough of our readings for now. Time to get our hands on the code.

You're free to set up your local installation as you want, but it is strongly recommended to use the [GitLab Development Kit (or GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit). 
If you recall the [GitLab architecture overview](https://docs.gitlab.com/ee/development/architecture.html), we can notice that GitLab is made up of many, many parts.

Installing and configuring each part one by one is possible _but_ that's exactly the goal of GDK: it will install, configure and maintain all the parts for you. There is even a [one-line installation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation).

Without further ado, let's:
- [ ] [Install GDK](https://gitlab.com/gitlab-org/gitlab-development-kit#installation).

Once GDK is installed, set up the additional parts that we use in the Package Registry. 

- [ ] [Object Storage](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/object_storage.md).

In the `gitlab` repository (the rails backend), let's
- [ ] Setup the [pre-push static analysis git hook](https://docs.gitlab.com/ee/development/contributing/style_guides.html#pre-push-static-analysis-with-lefthook).
  - GitLab rails comes with quite a large set of rubocop/linter rules. Some of them can be executed _before_ pushing commits.
  - Bonus points: set up your code editor to have rubocop rules executed on file save and mark offenses in the code.


### :popcorn: Let's play around

Now that we have everything set up, let's play around with our local GDK instance.

Package Registry:
- [ ] You will need a Project for this part. [Create one project](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project) in your local instance.
- [ ] [Create](https://docs.gitlab.com/ee/user/packages/npm_registry/#create-a-project) an NPM package and [publish](https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package) it in your local GDK instance.
- [ ] Open the Project on your local GDK instance and browse the Package Registry. Your shiny new package should be listed there. Click on it to see more details.
- [ ] Bonus points: explore the NPM API endpoints and their related classes.
- [ ] Bonus points: Publish a [package from a CI pipeline](https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package-by-using-cicd).
   - You will need to have set up a [Runner](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/runner.md) locally.
 

## :hammer_pick: Time to contribute!

Did you make this far? Congratulations! :checkered_flag: 

You now have all the basics to start contributing!

Congratulations and welcome to the Package registry!

<!-- DO NOT REMOVE -->
/confidential
/label ~"group::package" ~"section::ops" ~"devops::package" ~onboarding
/assignee [update here to put the manager and new team member]


<!-- Instructions:
[ ] Update title with new contractor
[ ] In the quick actions change the assignee to the new team member
[ ] Update list of links above

