## Rahul, Welcome to the Package Frontend!

We're all excited you'll be joining the Package stage and working on the frontend for Package and Container Registry. This document is intended to be a reference point for you as you go through your technical engineering onboarding and start to ramp up over the first couple of months. Consider this document a reference. There are a lot of links here to various issues and pages that will be useful as you get comfortable with the project. This is **your** issue, so feel free to make changes, add comments, or keep track of anything you want to review with your manager.

For general Package onboarding information, please refer to your [Package stage onboarding issue][package-stage-onboarding-issue].


### Documentation

Please read the following helpful links to learn more about the ecosystem that
the Package frontend is part of, as well architecture and guides specific to GitLab

1. [ ] [Frontend development guidelines](https://docs.gitlab.com/ce/development/fe_guide/)
1. [ ] [Vue documentation](https://vuejs.org/v2/guide/)
1. [ ] [GitLab UI](https://gitlab-org.gitlab.io/gitlab-ui)
1. [ ] [GitLab's Design System](http://design.gitlab.com)
1. [ ] GitLab uses [jest](https://jestjs.io/docs/getting-started) to run our `frontend` tests the command to run the tests in watch mode is: `yarn jest --watch`
1. [ ] [Frontend architecture for registry applications](https://docs.gitlab.com/ee/development/fe_guide/registry_architecture.html)


### Local Development Environment

You're free to set up your local installation as you want, but it is strongly recommended to use the [GitLab Development Kit (or GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit). 
If you recall the [GitLab architecture overview](https://docs.gitlab.com/ee/development/architecture.html), we can notice that GitLab is made up of many, many parts.

Installing and configuring each part one by one is possible _but_ that's exactly the goal of GDK: it will install, configure and maintain all the parts for you. There is even a [one-line installation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation).


- [ ] [Install GDK](https://gitlab.com/gitlab-org/gitlab-development-kit#installation).
- [ ] Once your environment is running, experiment with the interface
- [ ] Useful aliases to add to your `.bashrc/.zshrc`
  - `alias backend_tests="bundle exec spring rspec"`
  - `alias regenerate_graphql_docs="bundle exec rails gitlab:graphql:compile_docs gitlab:graphql:schema:dump"`
  - `alias frontend_tests="yarn jest --watch"`


### Operational tasks

- [ ] Join the [#frontend](https://gitlab.slack.com/messages/frontend/) channel on Slack
- [ ] Make sure to have the [Frontend calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7tt4l055h50dgucid0bth5dfl8%40group.calendar.google.com) in your google calendar

#### Working with Product Design

You'll be working closely with product design, so here are some tips to help you get started.

- [ ] Set up a coffee chat with Package product designer Katie Macoy
- [ ] Read about the [Product design process](https://about.gitlab.com/handbook/engineering/ux/product-designer/#product-design-process) at GitLab
- [ ] Set up a [Figma](https://www.figma.com/signup) account with your GitLab email address. This will allow you to view and comment on designs.
- [ ] Add the [Package customer sessions calendar](https://calendar.google.com/calendar/u/0?cid=Y19oazNsbHA0ZnVwMTNiNGlsNHM5NmRrZzNsY0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) to your Google Calendar. This shows you upcoming user research calls or calls with customers for you to observe. 

Consider joining the following slack channels:
- [ ] #ux_ci-cd channel to keep up-to-date on design and research initiatives in CI/CD
- [ ] #s_package_research to see what user research is happening in the Package stage
- [ ] #ux to keep up-to-date on what the entire UX department is up to. 

**Tips for working with product design** 
- See what research and design work is in flight in Package on the [validation track board](https://gitlab.com/groups/gitlab-org/-/boards/1397751?label_name[]=group%3A%3Apackage)
- Any user-facing changes in MRs need to be reviewed by a product designer. Make sure to tag your product designer as a reviewer for MRs. 
- Product designers follow [specific directions](https://about.gitlab.com/handbook/engineering/ux/product-designer/mr-reviews/) when reviewing MRs. When creating an MR that a product designer will review, make sure to include the following:
    - A thorough explanation of the changes.
    - How you can test the changes.
    - Link(s) to the related issue(s).
    - Before and After screenshots/videos

---

### Getting Started Tasks

Here are a couple of tasks that I've found that you can take on when you're ready to start getting into code. These should be reasonably straightforward, but let me know if they turn out to be too complex. These first tasks aren't intended to be complex features; they are meant to be simple tasks that will help you get familiar with the process of shipping code at GitLab.

[add 2 to 3 getting started issues]


### What's next?


After completing your onboarding and getting started tasks, you should feel comfortable to begin contributing normally to the teams' milestone efforts.
Feel free to reach out to your manager or peers for guidance if you don't know how/what to do.

- [ ] Review the current milestone's planning issue and pick an issue to start working on it!
- [ ] Close this issue 🎉

Congratulations and welcome to the Package stage!

---

### Additional Notes

Feel free to edit this issue and add any notes you'd like to keep track of or add comments below.



<!-- Instructions:
[ ] Title this issue as Name's onboarding to Package Frontend
[ ] Update title with the new team member name
[ ] In the section "Getting started" include a list of 2 to 3 issues
[ ] Below fill the list of links
[ ] In the quick actions add the manager, buddy and team member handlers
[ ] Add the due date to the issue to 3 months after starting -->

<!-- DO NOT REMOVE -->
[package-stage-onboarding-issue]: xxx <!-- replace xxx with the onboarding issue link -->

/confidential
/label ~"group::package" ~"section::ops" ~"devops::package" ~onboarding
/assign <!-- replace this for the team member handler -->
/due <!-- put here the due date -->
